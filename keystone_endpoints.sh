#!/bin/sh

# Author:       Martin Gerhard Loschwitz
# (c) 2012      hastexo Professional Services GmbH

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# On Debian-based systems the full text of the Apache version 2.0 
# license can be found in `/usr/share/common-licenses/Apache-2.0'.


## create all the needed services
keystone service-create --name "$KEYSTONE_SERVICE_NOVA_NAME" --type compute --description 'OpenStack Compute Service'
keystone service-create --name "$KEYSTONE_SERVICE_VOLUME_NAME" --type volume --description 'OpenStack Volume Service'
keystone service-create --name "$KEYSTONE_SERVICE_GLANCE_NAME" --type image --description 'OpenStack Image Service'
keystone service-create --name "$KEYSTONE_SERVICE_SWIFT_NAME" --type object-store --description 'OpenStack Storage Service'
keystone service-create --name "$KEYSTONE_SERVICE_KEYSTONE_NAME" --type identity --description 'OpenStack Identity'
keystone service-create --name "$KEYSTONE_SERVICE_EC2_NAME" --type ec2 --description 'OpenStack EC2 service'


create_endpoint () {
  case $1 in
    compute)
    keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SERVICE_IP"':8774/v2/%(tenant_id)s' --adminurl 'http://'"$SERVICE_IP"':8774/v2/%(tenant_id)s' --internalurl 'http://'"$SERVICE_IP"':8774/v2/%(tenant_id)s'
    ;;
    volume)
    keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SERVICE_IP"':8776/v1/%(tenant_id)s' --adminurl 'http://'"$SERVICE_IP"':8776/v1/%(tenant_id)s' --internalurl 'http://'"$SERVICE_IP"':8776/v1/%(tenant_id)s'
    ;;
    image)
    keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SERVICE_IP"':9292/v1' --adminurl 'http://'"$SERVICE_IP"':9292/v1' --internalurl 'http://'"$SERVICE_IP"':9292/v1'
    ;;
    object-store)
    if [ $SWIFT_MASTER ]; then
      keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SWIFT_MASTER"':8080/v1/AUTH_%(tenant_id)s' --adminurl 'http://'"$SWIFT_MASTER"':8080/v1' --internalurl 'http://'"$SWIFT_MASTER"':8080/v1/AUTH_%(tenant_id)s'
    else
      keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SERVICE_IP"':8080/v1/AUTH_%(tenant_id)s' --adminurl 'http://'"$SERVICE_IP"':8080/v1' --internalurl 'http://'"$SERVICE_IP"':8080/v1/AUTH_%(tenant_id)s'
    fi
    ;;
    identity)
    keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SERVICE_IP"':5000/v2.0' --adminurl 'http://'"$SERVICE_IP"':35357/v2.0' --internalurl 'http://'"$SERVICE_IP"':5000/v2.0'
    ;;
    ec2)
    keystone endpoint-create --region $KEYSTONE_REGION --service_id $2 --publicurl 'http://'"$SERVICE_IP"':8773/services/Cloud' --adminurl 'http://'"$SERVICE_IP"':8773/services/Admin' --internalurl 'http://'"$SERVICE_IP"':8773/services/Cloud'
    ;;
  esac
}


for i in compute volume image object-store identity ec2; do
  id=`mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" -ss -e "SELECT id FROM service WHERE type='"$i"';"` || exit 1
  create_endpoint $i $id
done

